#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setupMainApp();
    mainApp();
}

MainWindow::~MainWindow()
{
    delete ui;
}

/* Setup inicial dos widgets */
void MainWindow::setupMainApp(void)
{
    /* Timer */
    ui->timer_timeEdit->setEnabled(false);
    ui->timer_pushButton_send->setEnabled(false);

    /* Porta Serial */
    /* Evento: tratamento de bytes disponíveis na COM */
    connect(&serialPort, &QSerialPort::readyRead, this, &MainWindow::serialReadData);
    ui->serialPort_pushButton_open->setEnabled(true);
    ui->serialPort_pushButton_close->setEnabled(false);
    ui->serialPort_comboBox->setEnabled(true);
    setupSerialPort();
}

/* Setup inicial da porta serial */
void MainWindow::setupSerialPort(void)
{
    /* Listagem de COM na comboBox */
    ui->serialPort_comboBox->clear();
    foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        ui->serialPort_comboBox->addItem(info.portName());
    }
}

/* APP principal */
void MainWindow::mainApp(void)
{
    /* Do nothing */
}

/* EVENTO: Abrir serial Port */
void MainWindow::on_serialPort_pushButton_open_clicked()
{
    serialPort.setPortName(ui->serialPort_comboBox->currentText());

    /* Ajuste das configurações da COM */
    serialPort.setBaudRate(QSerialPort::Baud115200);
    serialPort.setDataBits(QSerialPort::Data8);
    serialPort.setParity(QSerialPort::NoParity);
    serialPort.setStopBits(QSerialPort::OneStop);
    serialPort.setFlowControl(QSerialPort::NoFlowControl);

    if(serialPort.open(QIODevice::ReadWrite))
    {
        ui->textBrowser->setText("<html><b>Serial</html></b>: Porta Serial-USB iniciada com sucesso.");
        ui->serialPort_pushButton_close->setEnabled(true);
        ui->serialPort_pushButton_open->setEnabled(false);
        ui->serialPort_comboBox->setEnabled(false);

        /* Timer */
        ui->timer_timeEdit->setTime(QTime());
        ui->timer_timeEdit->setEnabled(true);
        ui->timer_timeEdit->setFocus();
        ui->timer_pushButton_send->setEnabled(true);
    }
    else
    {
        ui->textBrowser->setText("<html><b>Serial</html></b>: Erro ao abrir porta Serial-USB.");
    }
}

/* EVENTO: Fechar serial Port */
void MainWindow::on_serialPort_pushButton_close_clicked()
{
    serialPort.clear();
    serialPort.close();

    ui->textBrowser->setText("<html><b>Serial</html></b>: Porta Serial-USB finalizada com sucesso.");

    /* widgets da porta serial */
    setupSerialPort();
    ui->serialPort_pushButton_open->setEnabled(true);
    ui->serialPort_pushButton_close->setEnabled(false);
    ui->serialPort_comboBox->setEnabled(true);

    /* Timer */
    ui->timer_timeEdit->setEnabled(false);
    ui->timer_pushButton_send->setEnabled(false);
}

/* EVENTO: Leitura da porta COM */
void MainWindow::serialReadData(void)
{
    serialPortBuffer.append(serialPort.readAll());

    if(serialPortBuffer.contains("\n"))
    {
        processCommand();
        serialPortBuffer.clear();
    }
}

void MainWindow::on_timer_pushButton_send_clicked()
{
    if(!serialPort.isOpen())
        return;

    serialPort.write("\n");
    time = QTime::currentTime();
    ui->textBrowser->setText("<html><b>Serial</html></b>: Comando enviado.");
}

void MainWindow::processCommand(void)
{

    if(serialPortBuffer.contains("Digite as horas:"))
    {
       serialPort.write(time.toString("hh").toLatin1());
       return;
    }
    else if(serialPortBuffer.contains("Digite os minutos:"))
    {
        serialPort.write(time.toString("mm").toLatin1());
        return;
    }
    else if(serialPortBuffer.contains("Digite os segundos:"))
    {
        serialPort.write(time.toString("ss").toLatin1());
        return;
    }
    else if(serialPortBuffer.contains("Horario ajustado!"))
    {
        ui->textBrowser->setText("<html><b>Serial</html></b>: Horário atual ajustado.");
        return;
    }
    else if(serialPortBuffer.contains("hora do alarme:"))
    {
        serialPort.write(ui->timer_timeEdit->time().toString("hh").toLatin1());
        ui->textBrowser->setText("<html><b>Serial</html></b>: Hora enviada.");
        return;
    }
    else if(serialPortBuffer.contains("Digite os minutos do alarme:"))
    {
        serialPort.write(ui->timer_timeEdit->time().toString("mm").toLatin1());
        ui->textBrowser->setText("<html><b>Serial</html></b>: minuto enviado.");
        return;
    }
    else if(serialPortBuffer.contains("Alarme ajustado!"))
    {
        ui->textBrowser->setText("<html><b>Serial</html></b>: Alarme ajustado.");
        return;
    }
    else
        return;
}
