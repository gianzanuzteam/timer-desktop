#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_serialPort_pushButton_open_clicked();
    void on_serialPort_pushButton_close_clicked();
    void on_timer_pushButton_send_clicked();

private:
    Ui::MainWindow *ui;
    void setupMainApp(void);
    void mainApp(void);
    void setupSerialPort(void);
    void processCommand(void);
    void serialReadData(void);

    QSerialPort serialPort{};
    QByteArray serialPortBuffer{};
    QTime time;
};

#endif // MAINWINDOW_H
